%INITO3PAL adds o3pal subfolder to matlab path
%
%   run:
%       INITO3PAL
%


%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nuernberg
%   Date:       11-Mar-2020


function initO3PAL()

    rootFolder = fileparts(mfilename('fullpath'));

    addpath([rootFolder,'/baseRecon']);
    addpath([rootFolder,'/linearOperatorRecon']);
    addpath([rootFolder,'/imageDenoising']);
    addpath([rootFolder,'/ooCore']);
    addpath([rootFolder,'/ooCore/helper']);
    addpath([rootFolder,'/ooCore/costFunctions']);

end

