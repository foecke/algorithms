classdef oooKaczmarz < handle & oooIterative
  
    methods (Access = public)
        
        
        function obj = oooKaczmarz()
            obj@oooIterative();
        end
        
        
        function obj = setKaczmarzWeightMatrix(obj, weightMatrix)
            obj.i.kacz.weightMatrix     = weightMatrix;
            obj.i.kacz.weightCumulated  = cumsum(obj.i.kacz.weightMatrix, 2);
            obj.i.kacz.weightMatrixSet  = true;
        end
        
        
    end
    
    methods (Access = protected)
        
        function obj = routineStartup(obj) 
            obj.i.kbs = obj.i.M / obj.s.kacz.numBlocks; %kaczmarz block size
            if floor(obj.i.kbs) ~= obj.i.kbs
                error('number of kaczmarz blocks is not compatible');
            end
            if ~obj.i.kacz.weightMatrixSet % subsequent kaczmarz (default)
                obj.setKaczmarzWeightMatrix(circshift(speye(obj.s.kacz.numBlocks),1,2));
            end
        end
        
        function obj = initializeAlg(obj)
            obj.s.kacz.numBlocks    = 1;
            obj.s.kacz.blockiter    = 1;
            obj.i.kacz.curIndex     = 1;
            
            obj.i.kacz.weightMatrixSet  = false;
            obj.i.kacz.weightMatrix     = [];
        end
        
        
        function obj = updateKaczmarzIndex(obj)
            if mod(obj.o.iteration, obj.s.kacz.blockiter) == 0
                obj.i.kacz.curIndex = find(obj.i.kacz.weightCumulated(obj.i.kacz.curIndex,:) > rand, 1, 'first');
            end
        end    
        
        
        function obj = deriveCurrentBlocks(obj)
            for k = 1:obj.s.kacz.numBlocks
                obj.i.curACell{k} = obj.i.A((k-1)*obj.i.kbs+(1:obj.i.kbs),:);
                obj.i.curBCell{k} = obj.i.b((k-1)*obj.i.kbs+(1:obj.i.kbs));
            end
        end
            
        
        function obj = setCurrentBlock(obj)
            if mod(obj.o.iteration, obj.s.kacz.blockiter) == 0
                obj.i.curA = obj.i.curACell{obj.i.kacz.curIndex};
                obj.i.curB = obj.i.curBCell{obj.i.kacz.curIndex};
            end
        end
        
        
        function obj = recordCustomFunctions(obj)
            obj.o.record(obj.i.record.iter).kaczIndex  	= obj.i.kacz.curIndex;
        end        
        
        
        function obj = routineCheckStopCrit(obj)
            routineCheckStopCrit@oooIterative(obj);
            if isfield(obj.o.record, 'kaczIndex')
                l2ki = find([obj.o.record.kaczIndex] == 18, 2, 'last'); %last two kaczmarz indezies
                if numel(l2ki) > 1
                    if abs(obj.o.record(l2ki(1)).cost - obj.o.record(l2ki(2)).cost) < obj.s.tolRel
                        obj.i.stop = 8;
                    end
                end
            end
        end
        
        
    end
    
end