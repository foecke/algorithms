
classdef oocf_L2odata < oooRoot
    
    methods (Access = public)
        
        
        function out = costFunction(obj, x)
            out = (0.5 .* norm(obj.i.A*x(:)-obj.i.b).^2) ./ numel(x(:));
        end
        
        
    end
    
end
