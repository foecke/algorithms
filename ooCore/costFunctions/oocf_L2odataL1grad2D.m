
classdef oocf_L2odataL1grad2D < oooRoot
    
    methods (Access = public)
        
        
        function out = costFunction(obj, x)
            out = ( 0.5 .* norm(obj.i.A*x(:)-obj.i.b).^2 + obj.p.alpha .* norm(obj.i.gradX*x(:)+obj.i.gradY*x(:), 1) ) ./ numel(x(:));
        end
        
        
    end
    
end
