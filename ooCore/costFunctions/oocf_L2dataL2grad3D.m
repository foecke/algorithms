
classdef oocf_L2dataL2grad3D < oooRoot
    
    methods (Access = public)
        
        
        function out = costFunction(obj, x)
            out = (0.5 .* norm(x(:)-obj.i.b).^2 + 0.5 .* obj.p.alpha .* norm(obj.i.gradX*x(:) + obj.i.gradY*x(:) + obj.i.gradZ*x(:)).^2) ./ numel(x(:));
        end
        
        
    end
    
end
