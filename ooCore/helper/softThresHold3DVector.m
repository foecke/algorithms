
%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen
%   Date:       13-Oct-2017

function [Svx,Svy,Svz]  = softThresHold3DVector(vx,vy,vz,alpha)

s = sqrt(vx.^2+vy.^2+vz.^2);

pos_s_ind = s > 0;

factor = max(abs(s)-alpha,0);
factor(pos_s_ind) = factor(pos_s_ind)./s(pos_s_ind);

Svx = factor .* vx;
Svy = factor .* vy;
Svz = factor .* vz;

end
