
classdef ooL2odataL2idND_i_GDi_Kacz < ooL2odataL2idND_i_GD_Kacz
    
    methods (Access = public)
        
        
        function obj = ooL2odataL2idND_i_GDi_Kacz(A, b, res, varargin)
            obj@ooL2odataL2idND_i_GD_Kacz(A, b, res, varargin{:});
            obj.setAlgoName('ooL2odataL2idND_i_GDi_Kacz');
        end
        
        
    end
    
    methods (Access = protected)
        
        
        function obj = routineUpdateStep(obj)
            if obj.s.kacz.numBlocks > 1 && obj.o.iteration > 1
                obj.updateKaczmarzIndex();
                obj.setCurrentBlock();
            end
            obj.setEffectiveA();
            obj.setRightHandSide();       
            
            obj.o.result = obj.i.effA \ obj.i.rhs;
            if obj.p.nonNeg
                obj.o.result = max(obj.o.result, 0); 
            end
        end
        
        
        function obj = setEffectiveA(obj)
            obj.i.effA = obj.i.curGamma .* ( obj.i.curA'*obj.i.curA + (obj.p.alpha./ obj.s.kacz.numBlocks) .* speye(obj.i.N) ) + speye(obj.i.N);
        end
        
        
        function obj = setRightHandSide(obj)
            obj.i.rhs = obj.i.curGamma .* (obj.i.curA'*obj.i.curB) + obj.o.result;
        end
        
        
    end
    
end
