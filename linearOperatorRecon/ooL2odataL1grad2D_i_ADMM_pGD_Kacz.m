
classdef ooL2odataL1grad2D_i_ADMM_pGD_Kacz < oooKaczmarz & ooL2odataL1grad2D_i_ADMM_pGD
    
    methods (Access = public)
        
        
        function obj = ooL2odataL1grad2D_i_ADMM_pGD_Kacz(A, b, res, varargin)
            obj@ooL2odataL1grad2D_i_ADMM_pGD(A, b, res, varargin{:});
            obj.setAlgoName('ooL2odataL1grad2D_i_ADMM_pGD_Kacz');
        end
        
        
    end
    
    methods (Access = protected)
        
        
        function obj = initializeAlg(obj)         
            initializeAlg@oooKaczmarz(obj);
            initializeAlg@ooL2odataL1grad2D_i_ADMM_pGD(obj);
        end
        
        
        function obj = setCurrentBlock(obj)
            setCurrentBlock@oooKaczmarz(obj);
            if numel(obj.p.gamma) == obj.s.kacz.numBlocks
                obj.i.curGamma = obj.p.gamma(obj.i.kacz.curIndex);
            else
                obj.i.curGamma = obj.p.gamma(1);
            end
        end
        
        
        function obj = routineStartup(obj) 
            routineStartup@oooKaczmarz(obj);
            obj.deriveCurrentBlocks();
            obj.setCurrentBlock();
        end
        
        
        function obj = routineUpdateStep(obj)   
            if obj.s.kacz.numBlocks > 1 && obj.o.iteration > 1
                obj.updateKaczmarzIndex();
                obj.setCurrentBlock();
            end
            obj.setEffectiveA();
            obj.setRightHandSide();
            
            obj.o.result = obj.o.result - obj.i.curGamma * (obj.i.effA - obj.i.rhs);
            
            obj.i.fx = obj.i.gradX * obj.o.result;
            obj.i.fy = obj.i.gradY * obj.o.result;
            obj.i.fp = obj.o.result;
            
            obj.i.vxOld = obj.i.vx;
            obj.i.vyOld = obj.i.vy;
            obj.i.vpOld = obj.i.vp;
            
            if strcmp(obj.p.tv, 'aniso')
                obj.i.vx = softThresHold1DVector(obj.i.fx+obj.i.wx,obj.p.alpha/obj.p.rho);
                obj.i.vy = softThresHold1DVector(obj.i.fy+obj.i.wy,obj.p.alpha/obj.p.rho);
            else
                [obj.i.vx,obj.i.vy] = softThresHold2DVector(obj.i.fx+obj.i.wx,obj.i.fy+obj.i.wy,obj.p.alpha/obj.p.rho);
            end
            obj.i.vp = max(obj.i.wp+obj.i.fp, 0);
            
            obj.i.wx = obj.i.wx + obj.i.fx - obj.i.vx;
            obj.i.wy = obj.i.wy + obj.i.fy - obj.i.vy;
            obj.i.wp = obj.i.wp + obj.i.fp - obj.i.vp;
        end
                
        
        function obj = recordCustomFunctions(obj)
            obj.recordCustomFunctions@ooL2odataL1grad2D_i_ADMM_pGD();
            obj.recordCustomFunctions@oooKaczmarz();
        end
        
        
        function obj = routineCheckStopCrit(obj)
            obj.routineCheckStopCrit@ooL2odataL1grad2D_i_ADMM_pGD();
            obj.routineCheckStopCrit@oooKaczmarz();
        end
        
        
        function obj = setRightHandSide(obj)
            if obj.p.nonNeg
                obj.i.rhs = obj.i.curA'*obj.i.curB + (obj.p.rho ./ obj.s.kacz.numBlocks) .* (obj.i.gradX'*(obj.i.vx(:)-obj.i.wx(:)) + obj.i.gradY'*(obj.i.vy(:)-obj.i.wy(:)) + (obj.i.vp(:)-obj.i.wp(:)) );
            else
                obj.i.rhs = obj.i.curA'*obj.i.curB + (obj.p.rho ./ obj.s.kacz.numBlocks) .* (obj.i.gradX'*(obj.i.vx(:)-obj.i.wx(:)) + obj.i.gradY'*(obj.i.vy(:)-obj.i.wy(:)) );
            end
            
        end
        
        
        function obj = setEffectiveA(obj)
            if obj.p.nonNeg
                obj.i.effA = obj.i.curA'*(obj.i.curA * obj.o.result) + (obj.p.rho ./ obj.s.kacz.numBlocks) .* ( obj.i.gradX' * (obj.i.gradX * obj.o.result) + obj.i.gradY' * (obj.i.gradY * obj.o.result) + obj.o.result );
            else
                obj.i.effA = obj.i.curA'*(obj.i.curA * obj.o.result) + (obj.p.rho ./ obj.s.kacz.numBlocks) .* ( obj.i.gradX' * (obj.i.gradX * obj.o.result) + obj.i.gradY' * (obj.i.gradY * obj.o.result) );
            end
        end
        
        
    end
    
end
