
classdef ooL2odataL2grad2D_i_ADMM_pGD_Kacz < oooKaczmarz & ooL2odataL2grad2D_i_ADMM_pBS
    methods (Access = public)
        function obj = ooL2odataL2grad2D_i_ADMM_pGD_Kacz(A, b ,res, varargin)
            obj@ooL2odataL2grad2D_i_ADMM_pBS(A, b, res, varargin{:});
            obj.setAlgoName('ooL2odataL2grad2D_i_ADMM_pGD_Kacz');
        end
        
        
    end
    
    methods (Access = protected)
          
        
        function obj = initializeAlg(obj)         
            initializeAlg@oooKaczmarz(obj);
            initializeAlg@ooL2odataL2grad2D_i_ADMM_pBS(obj);
        end     
        
        
        function obj = setCurrentBlock(obj)
            setCurrentBlock@oooKaczmarz(obj);
            if numel(obj.p.gamma) == obj.s.kacz.numBlocks
                obj.i.curGamma = obj.p.gamma(obj.i.kacz.curIndex);
            else
                obj.i.curGamma = obj.p.gamma(1);
            end
        end
        
        
        function obj = routineStartup(obj) 
            routineStartup@oooKaczmarz(obj);
            obj.deriveCurrentBlocks();
            obj.setCurrentBlock();
        end
        
        
        function routineUpdateStep(obj)  
            if obj.s.kacz.numBlocks > 1 && obj.o.iteration > 1
                obj.updateKaczmarzIndex();
                obj.setCurrentBlock();
            end
            obj.setEffectiveA();
            obj.setRightHandSide();
            
            obj.o.result    = obj.o.result - obj.i.curGamma * (obj.i.effA - obj.i.rhs);
            
            obj.i.fp        = obj.o.result;            
            obj.i.vpOld     = obj.i.vp;
            obj.i.vp        = max(obj.i.wp+obj.i.fp, 0);            
            obj.i.wp        = obj.i.wp + obj.i.fp - obj.i.vp;            
        end
        
                
        function obj = adaptiveStep(obj)            
            obj = adaptiveStep@oooADMM(obj);            
            obj.i.wp = obj.i.wp*(obj.i.rhoOld/obj.p.rho);
        end
        
        
        function obj = recordCustomFunctions(obj)
            obj.recordCustomFunctions@ooL2odataL2grad2D_i_ADMM_pBS();
            obj.recordCustomFunctions@oooKaczmarz();
        end
        
        
        function obj = routineCheckStopCrit(obj)
            obj.routineCheckStopCrit@ooL2odataL2grad2D_i_ADMM_pBS();
            obj.routineCheckStopCrit@oooKaczmarz();
        end
        
        
        function obj = setRightHandSide(obj) 
            if obj.p.nonNeg
                obj.i.rhs   = obj.i.curA'*obj.i.curB + (obj.p.rho ./ obj.s.kacz.numBlocks) .* (obj.i.vp(:)-obj.i.wp(:));
            else
                obj.i.rhs   = obj.i.curA'*obj.i.curB;
            end
        end
        
        
        function obj = setEffectiveA(obj) 
            if obj.p.nonNeg
                obj.i.effA  = obj.i.curA'*(obj.i.curA * obj.o.result) + (obj.p.alpha ./ obj.s.kacz.numBlocks) .* ( obj.i.gradX' * (obj.i.gradX * obj.o.result) + obj.i.gradY' * (obj.i.gradY * obj.o.result) ) + (obj.p.rho ./ obj.s.kacz.numBlocks) .* obj.o.result;
            else
                obj.i.effA  = obj.i.curA'*(obj.i.curA * obj.o.result) + (obj.p.alpha ./ obj.s.kacz.numBlocks) .* ( obj.i.gradX' * (obj.i.gradX * obj.o.result) + obj.i.gradY' * (obj.i.gradY * obj.o.result) );
            end
        end
        
        
    end
end
