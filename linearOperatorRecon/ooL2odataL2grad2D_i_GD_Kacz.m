
classdef ooL2odataL2grad2D_i_GD_Kacz < oooKaczmarz & ooL2odataL2grad2D_i_GD
    
    methods (Access = public)
        
        
        function obj = ooL2odataL2grad2D_i_GD_Kacz(A, b, res, varargin)
            obj@ooL2odataL2grad2D_i_GD(A, b, res, varargin{:});
            obj.setAlgoName('ooL2odataL2grad2D_i_GD_Kacz');
        end
        
        
    end
    
    methods (Access = protected)
        
        
        function initializeAlg(obj)            
            initializeAlg@ooL2odataL2grad2D_i_GD(obj);
            initializeAlg@oooKaczmarz(obj);
        end
        
        
        function obj = setCurrentBlock(obj)
            setCurrentBlock@oooKaczmarz(obj);
            if numel(obj.p.gamma) == obj.s.kacz.numBlocks
                obj.i.curGamma = obj.p.gamma(obj.i.kacz.curIndex);
            else
                obj.i.curGamma = obj.p.gamma(1);
            end
        end
        
        
        function obj = routineStartup(obj)
            routineStartup@oooKaczmarz(obj);
            obj.deriveCurrentBlocks();
            obj.setCurrentBlock(); 
        end
        
        
        function obj = routineUpdateStep(obj)
            if obj.s.kacz.numBlocks > 1 && obj.o.iteration > 1
                obj.updateKaczmarzIndex();
                obj.setCurrentBlock();
            end
            
            obj.o.result = obj.o.result - obj.i.curGamma .* (obj.i.curA'*(obj.i.curA*obj.o.result) - obj.i.curA' * obj.i.curB + (obj.p.alpha./ obj.s.kacz.numBlocks) .* (obj.i.gradX'*(obj.i.gradX*obj.o.result) + obj.i.gradY'*(obj.i.gradY*obj.o.result)));
            if obj.p.nonNeg
                obj.o.result = max(obj.o.result, 0); 
            end
        end
        
        
    end
    
end
