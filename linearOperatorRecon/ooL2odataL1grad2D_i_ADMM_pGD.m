
classdef ooL2odataL1grad2D_i_ADMM_pGD < ooL2odataL1grad2D_i_ADMM_pBS
    
    methods (Access = public)
        
        
        function obj = ooL2odataL1grad2D_i_ADMM_pGD(A, b, res, varargin)
            obj@ooL2odataL1grad2D_i_ADMM_pBS(A, b, res, varargin{:});
            obj.setAlgoName('ooL2odataL1grad2D_i_ADMM_pGD');
        end
        
        
    end
    
    methods (Access = protected)
        
        
        function obj = initializeAlg(obj)            
            initializeAlg@oooADMM(obj);
            
            obj.p.alpha        	= 1e-1;
            obj.p.rho        	= 1e-2;
            obj.p.tv            = 'iso';
            obj.p.nonNeg        = true;
            
            obj.i.N             = prod(obj.i.res);
            obj.i.M             = numel(obj.i.b);
            obj.o.result        = zeros(obj.i.N,1);
            
            obj.i.ATb           = obj.i.A'*obj.i.b;
            
            gradientMats        = genForwardGradient(obj.i.res, ones(numel(obj.i.res),1));
            obj.i.gradX         = gradientMats{1};
            obj.i.gradY         = gradientMats{2};
            obj.i.DAD           = obj.i.gradX'*obj.i.gradX+obj.i.gradY'*obj.i.gradY;
            
            obj.i.vx            = zeros(obj.i.N,1);
            obj.i.wx            = zeros(obj.i.N,1);
            obj.i.vy            = zeros(obj.i.N,1);
            obj.i.wy            = zeros(obj.i.N,1);
            obj.i.vp            = zeros(obj.i.N,1);
            obj.i.wp            = zeros(obj.i.N,1);
            
            obj.i.fx            = obj.i.gradX*obj.o.result;
            obj.i.fy            = obj.i.gradY*obj.o.result;
            obj.i.fp            = obj.o.result;
            
            obj.i.vxOld         = obj.i.vx;
            obj.i.vyOld         = obj.i.vy;
            obj.i.vpOld         = obj.i.vp;
        end
                
        
        function obj = routineUpdateStep(obj)   
            obj.setEffectiveA();
            obj.setRightHandSide();
            
            obj.o.result = obj.o.result - obj.p.gamma * (obj.i.effA - obj.i.rhs);
            
            obj.i.fx = obj.i.gradX * obj.o.result;
            obj.i.fy = obj.i.gradY * obj.o.result;
            obj.i.fp = obj.o.result;
            
            obj.i.vxOld = obj.i.vx;
            obj.i.vyOld = obj.i.vy;
            obj.i.vpOld = obj.i.vp;
            
            if strcmp(obj.p.tv, 'aniso')
                obj.i.vx = softThresHold1DVector(obj.i.fx+obj.i.wx,obj.p.alpha/obj.p.rho);
                obj.i.vy = softThresHold1DVector(obj.i.fy+obj.i.wy,obj.p.alpha/obj.p.rho);
            else
                [obj.i.vx,obj.i.vy] = softThresHold2DVector(obj.i.fx+obj.i.wx,obj.i.fy+obj.i.wy,obj.p.alpha/obj.p.rho);
            end
            obj.i.vp = max(obj.i.wp+obj.i.fp, 0);
            
            obj.i.wx = obj.i.wx + obj.i.fx - obj.i.vx;
            obj.i.wy = obj.i.wy + obj.i.fy - obj.i.vy;
            obj.i.wp = obj.i.wp + obj.i.fp - obj.i.vp;
        end
        
        
        function obj = adaptiveStep(obj)
            obj.adaptiveStep@oooADMM();
            obj.i.wx = obj.i.wx*(obj.i.rhoOld/obj.p.rho);
            obj.i.wy = obj.i.wy*(obj.i.rhoOld/obj.p.rho);
            obj.i.wp = obj.i.wp*(obj.i.rhoOld/obj.p.rho);
        end
        
        
        function obj = setRightHandSide(obj)
            if obj.p.nonNeg
                obj.i.rhs = obj.i.ATb + obj.p.rho .* (obj.i.gradX'*(obj.i.vx(:)-obj.i.wx(:)) + obj.i.gradY'*(obj.i.vy(:)-obj.i.wy(:)) + (obj.i.vp(:)-obj.i.wp(:)) );
            else
                obj.i.rhs = obj.i.ATb + obj.p.rho .* (obj.i.gradX'*(obj.i.vx(:)-obj.i.wx(:)) + obj.i.gradY'*(obj.i.vy(:)-obj.i.wy(:)) );
            end
        end
        
        
        function obj = setEffectiveA(obj)
            if obj.p.nonNeg
                obj.i.effA = obj.i.A'*(obj.i.A * obj.o.result) + obj.p.rho .* ( obj.i.gradX' * (obj.i.gradX * obj.o.result) + obj.i.gradY' * (obj.i.gradY * obj.o.result) + obj.o.result );
            else
                obj.i.effA = obj.i.A'*(obj.i.A * obj.o.result) + obj.p.rho .* ( obj.i.gradX' * (obj.i.gradX * obj.o.result) + obj.i.gradY' * (obj.i.gradY * obj.o.result) );
            end
        end
        
        
    end
    
end
