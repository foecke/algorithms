
classdef ooL2odataL1grad2D < ooL2odataL1grad2D_i_ADMM_pBS
        
    methods (Access = public)
        
        
        function obj = ooL2odataL1grad2D(A, b, res, varargin)
            obj@ooL2odataL1grad2D_i_ADMM_pBS(A, b, res, varargin{:});
        end
        
        
    end    
    
end
