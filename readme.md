# **O³PAL** - **O**bject-**O**riented **OP**timization **AL**gorithms

## the core: oooRoot

oooRoot is a matlab handle that adds a lot of functionality to the individual implemented algorithms. This manual will only discuss the properties of oooRoot. Implementations are available as submodules (later).

oooRoot uses the following internal variables:

 * **p** "*parameter*": parameter that change the optimization problem itself
 * **s** "*settings*": parameter that change the way the reconstruction algorithm works
 * **i** "*internals*": storage for internal variables, that are not relevant for the end user
 * **o** "*output variables*": variables, that are relevant for the end user

the variables in p and s are modifiable by the end user, i is just internal and may not be changed

## Iterative or non-iterative scheme

oooRoot can be used for iterative or non-iterative algorithms. Depending on the algorithms on of the following schemes is used.

### iterative algorithm scheme (oooIterative)

```
    obj.s.maxIter = obj.s.maxIter + numIter;
    obj.routineStop();
    if ~obj.i.stop
        obj.routineStartup();
        obj.i.overallTime = tic;
        obj.i.iterTime = tic;
        while obj.i.stop == 0
            obj.i.iteration=obj.i.iteration+1;
            obj.routineUpdateStep();
            obj.routineError();
            obj.routineAdaptive();
            obj.routineStop();
            obj.routinePrint();
            obj.saveResult(obj.i.iteration);
        end
        obj.routineFinish();
    else
        obj.s.maxIter = obj.s.maxIter - numIter;
    end
```

### non-iterative algorithm scheme (oooRoot)

```
    obj.routineStartup();
    obj.i.overallTime = tic;
    obj.i.iterTime = tic;
    obj.routineUpdateStep();
    obj.routineStop();
    obj.routinePrint();
    obj.routineFinish();
```

