%OOL2DATAL2GRAD2D
%   min_x -> 1/2 * || x - b ||_2^2 + alpha/2 * || \nabla x ||_2^2
%
%   Input:
%       b       2D noisy data
%       res     data resolution (required for gradient)
%
%   Parameter:
%       alpha   regularization parameter
%       tau     step size

%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen
%   Date:       16-Mar-2020

classdef ooL2dataL2grad2D_i < oooIterative & oooL2dataL2grad2D_costFunction
    methods (Access = public)
        function obj = ooL2dataL2grad2D_i(b,res, varargin)
            obj@oooIterative();
            obj.setAlgoName('ooL2dataL2grad2D_i');
            
            obj.i.b     = b;
            obj.i.res   = res;
            
            obj.initialize(varargin{:});
        end
        
        function result = getResult(obj)
            result = reshape(getResult@oooRoot(obj), obj.i.res);
        end
        
    end
    
    
    methods (Access = protected)
        
        function initializeAlg(obj)
            obj.p.alpha     = 5e-1;
            obj.p.gamma     = 1e-1;
            obj.p.nonNeg    = false;
            
            obj.i.N     	= prod(obj.i.res);
            obj.i.M       	= numel(obj.i.b);
            obj.o.result    = zeros(obj.i.N,1);
            
            gradientMats = genForwardGradient(obj.i.res, ones(numel(obj.i.res),1));
            obj.i.gradX = gradientMats{1};
            obj.i.gradY = gradientMats{2};
        end
        
        function routineUpdateStep(obj)
            obj.o.result = obj.o.result - obj.p.gamma .* (obj.o.result - obj.i.b + obj.p.alpha .* (obj.i.gradX'*(obj.i.gradX*obj.o.result) + obj.i.gradY'*(obj.i.gradY*obj.o.result)));
        end
    end
end

