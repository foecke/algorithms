%OOL2DATAL2GRAD2D
%   min_x -> 1/2 * || x - b ||_2^2 + alpha * || x ||_1
%
%   Input:
%       b       2D noisy data
%       res     data resolution (required for gradient)
%
%   Parameter:
%       alpha   regularization parameter
%       tau     step size

%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen
%   Date:       17-Mar-2020

classdef ooL2dataL1id2D_i_ADMM_pBS < oooADMM & oocf_L2dataL1idND
    methods (Access = public)
        function obj = ooL2dataL1id2D_i_ADMM_pBS(b,res, varargin)
            obj@oooADMM();
            obj.setAlgoName('ooL2dataL1id2D_i_ADMM_pBS');
            
            obj.i.b     = b;
            obj.i.res   = res;
            
            obj.initialize(varargin{:});
        end
        
        function result = getResult(obj)
            result = reshape(getResult@oooRoot(obj), obj.i.res);
        end
    end
    
    methods (Access = protected)
        
        function initializeAlg(obj)
                      
            initializeAlg@oooADMM(obj);
            
            obj.p.alpha        	= 1e-1;
            obj.p.rho        	= 1e-2;
            obj.p.nonNeg        = true;
            
            obj.i.N             = prod(obj.i.res);
            obj.o.result        = zeros(obj.i.N,1);
            
            obj.i.vx            = zeros(obj.i.N,1);
            obj.i.vy            = zeros(obj.i.N,1);
            obj.i.wx            = zeros(obj.i.N,1);
            obj.i.wy            = zeros(obj.i.N,1);
            obj.i.vp            = zeros(obj.i.N,1);
            obj.i.wp            = zeros(obj.i.N,1);
            
            obj.i.fx            = obj.o.result;
            obj.i.fy            = obj.o.result;
            obj.i.fp            = obj.o.result;
            
            obj.i.vxOld         = obj.i.vx;
            obj.i.vyOld         = obj.i.vy;
            obj.i.vpOld         = obj.i.vp;
        end
                
        function obj = routineStartup(obj) 
            obj.setEffectiveA();
        end
        
        function routineUpdateStep(obj)         
            obj.setRightHandSide();
            
            obj.o.result = obj.i.effA \ obj.i.rhs;
            
            obj.i.fx = obj.o.result;
            obj.i.fy = obj.o.result;
            obj.i.fp = obj.o.result;
            
            obj.i.vxOld = obj.i.vx;
            obj.i.vyOld = obj.i.vy;
            obj.i.vpOld = obj.i.vp;
            
            obj.i.vx = softThresHold1DVector(obj.i.fx+obj.i.wx,obj.p.alpha/obj.p.rho);
            obj.i.vy = softThresHold1DVector(obj.i.fy+obj.i.wy,obj.p.alpha/obj.p.rho);
            obj.i.vp = max(obj.i.wp+obj.i.fp, 0);
            
            obj.i.wx = obj.i.wx + obj.i.fx - obj.i.vx;
            obj.i.wy = obj.i.wy + obj.i.fy - obj.i.vy;
            obj.i.wp = obj.i.wp + obj.i.fp - obj.i.vp;
            
        end
                
        function obj = evaluateCustomCostFunction(obj)
            if obj.p.nonNeg
                obj.o.primalErr = norm([obj.i.fx-obj.i.vx;obj.i.fy-obj.i.vy;obj.i.fp-obj.i.vp]) ./ obj.i.N;
                obj.o.dualErr   = norm(-obj.p.rho * ((obj.i.vx-obj.i.vxOld)+(obj.i.vy-obj.i.vyOld)+(obj.i.vp-obj.i.vpOld))) ./ obj.i.N;
            else
                obj.o.primalErr = norm([obj.i.fx-obj.i.vx;obj.i.fy-obj.i.vy]) ./ obj.i.N;
                obj.o.dualErr   = norm(-obj.p.rho * (obj.i.vx-obj.i.vxOld)+(obj.i.vy-obj.i.vyOld)) ./ obj.i.N;
            end
            obj.o.nnegErr   = norm(obj.o.result(obj.o.result<0), inf)/norm(obj.o.result(obj.o.result>0));
        end
        
        function obj = adaptiveStep(obj)
            obj.adaptiveStep@oooADMM();
            obj.i.wx = obj.i.wx*(obj.i.rhoOld/obj.p.rho);
            obj.i.wy = obj.i.wy*(obj.i.rhoOld/obj.p.rho);
            obj.i.wp = obj.i.wp*(obj.i.rhoOld/obj.p.rho);
            obj.setEffectiveA();
        end
        
        
        function obj = setRightHandSide(obj)
            if obj.p.nonNeg
                obj.i.rhs = obj.i.b + obj.p.rho .* ((obj.i.vx(:)-obj.i.wx(:)) + (obj.i.vy(:)-obj.i.wy(:)) + (obj.i.vp(:)-obj.i.wp(:)) );
            else
                obj.i.rhs = obj.i.b + obj.p.rho .* ((obj.i.vx(:)-obj.i.wx(:)) + (obj.i.vy(:)-obj.i.wy(:)) );
            end            
        end
        
        
        function obj = setEffectiveA(obj)
            if obj.p.nonNeg
                obj.i.effA = speye(obj.i.N) + obj.p.rho .* (3 .* speye(obj.i.N));
            else
                obj.i.effA = speye(obj.i.N) + obj.p.rho .* (2 .* speye(obj.i.N));
            end
        end
        
    end
end

