
classdef ooL2dataL2grad3D_BS < oooRoot & oooL2dataL2grad3D_costFunction
    methods (Access = public)
        function obj = ooL2dataL2grad3D_BS(b, res, varargin)
            obj@oooRoot();
            obj.setAlgoName('ooL2dataL2grad3D_BS');
            
            obj.i.b     = b;
            obj.i.res   = res;
            
            obj.initialize(varargin{:});
        end
        
        function result = getResult(obj)
            result = reshape(getResult@oooRoot(obj), obj.i.res);
        end
        
    end
    
    
    methods (Access = protected)
        
        function initializeAlg(obj)
            obj.p.alpha     = 5e-1;
            obj.p.nonNeg    = false;
            
            obj.i.N     	= prod(obj.i.res);
            obj.i.M       	= numel(obj.i.b);
            obj.o.result    = zeros(obj.i.N,1);
            
            gradientMats = genForwardGradient(obj.i.res, ones(numel(obj.i.res),1));
            obj.i.gradX = gradientMats{1};
            obj.i.gradY = gradientMats{2};
        end
        
        function routineUpdateStep(obj)
            obj.setRightHandSide();
            obj.setEffectiveA();
            
            obj.o.result = obj.i.effA \ obj.i.rhs;
            if obj.p.nonNeg
                obj.o.result = max(obj.o.result, 0);
            end
        end
        
        
        function obj = setRightHandSide(obj)
            obj.i.rhs = obj.i.b;
        end
        
        
        function obj = setEffectiveA(obj)
            obj.i.effA = eye(obj.i.N) + obj.p.alpha .* ( obj.i.gradX'*obj.i.gradX + obj.i.gradY'*obj.i.gradY + obj.i.gradZ'*obj.i.gradZ );
        end
        
    end
end

