%OOL2DATAL2GRAD3D
%   min_x -> 1/2 * || x - b ||_2^2 + alpha * || \nabla x ||_1
%
%   Input:
%       b       3D noisy data
%       res     data resolution (required for gradient)
%
%   Parameter:
%       alpha   regularization parameter
%       tau     step size

%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen
%   Date:       17-Mar-2020

classdef ooL2dataL1grad3D_i < handle & oooIterative
    methods (Access = public)
        function obj = ooL2dataL1grad3D_i(b,res, varargin)
            obj@oooIterative();
            obj.setAlgoName('ooL2dataL1grad3D_i');
            
            obj.i.b     = b;
            obj.i.res   = res;
            
            obj.initialize(varargin{:});
        end
        
        function result = getResult(obj)
            result = reshape(getResult@oooRoot(obj), obj.i.res);
        end
    end
    
    methods (Access = protected)
        
        function initializeAlg(obj)
            obj.i.N             = prod(obj.i.res);
            obj.o.result        = zeros(obj.i.N,1);
            
            obj.p.alpha        	= 1e-1;
            obj.p.rho        	= 1e-2;
            obj.p.tv            = 'iso';
            
            obj.s.adaptiveSteps = true;
            obj.i.adaptiveWeight = 0.1;
            obj.i.adaptiveWeightWeight = 0.9;
            obj.i.adaptiveAdaptiveWeight = true;
            
            obj.o.primalErr     = 1;
            obj.o.dualErr       = 1;
            
            obj.s.verbosePattern= 'name: eval - iter - adaptive - cost - primal - dual - time - overall';
            
            gradientMats        = genForwardGradient(obj.i.res, ones(numel(obj.i.res),1));
            obj.i.gradX         = gradientMats{1};
            obj.i.gradY         = gradientMats{2};
            obj.i.gradZ         = gradientMats{3};
            
            obj.i.DAD           = obj.i.gradX'*obj.i.gradX+obj.i.gradY'*obj.i.gradY;
            obj.i.effA          = speye(obj.i.N) + obj.p.rho * obj.i.DAD;
            
            obj.i.vx            = zeros(obj.i.N,1);
            obj.i.wx            = zeros(obj.i.N,1);
            obj.i.vy            = zeros(obj.i.N,1);
            obj.i.wy            = zeros(obj.i.N,1);
            obj.i.vz            = zeros(obj.i.N,1);
            obj.i.wz            = zeros(obj.i.N,1);
            
            obj.i.fx            = obj.i.gradX*obj.o.result;
            obj.i.fy            = obj.i.gradY*obj.o.result;
            obj.i.fz            = obj.i.gradZ*obj.o.result;
            
            obj.i.vxOld         = obj.i.vx;
            obj.i.vyOld         = obj.i.vy;
            obj.i.vzOld         = obj.i.vz;
            
            obj.i.J             = @(x) obj.i.Jdata(x) + obj.p.alpha .* obj.i.JTV(x);
            obj.i.Jdata         = @(x) 0.5 .* norm(x-obj.i.b).^2;
            obj.i.JTV           = @(x) norm(x, 1);
        end
        
        function costFunction(obj)
            obj.o.currCost  = obj.i.J(obj.o.result) ./ obj.i.N;
            
            auxP            = [obj.i.fx-obj.i.vx;obj.i.fy-obj.i.vy;obj.i.fz-obj.i.vz];
            obj.o.primalErr = norm(auxP)./obj.i.N;
            
            auxD            = -obj.p.rho * (obj.i.gradX'*(obj.i.vx-obj.i.vxOld)+obj.i.gradY'*(obj.i.vy-obj.i.vyOld)+obj.i.gradZ'*(obj.i.vz-obj.i.vzOld));
            obj.o.dualErr   = norm(auxD)./obj.i.N;
            
        end
        
        function parseVerbosePattern(obj)
            
            parseVerbosePattern@oooIterative(obj);
            
            obj.i.verboseString = strrep(obj.i.verboseString, 'primal', sprintf('P: %06i', obj.o.primalErr));
            obj.i.verboseString = strrep(obj.i.verboseString, 'dual'  , sprintf('D: %06i', obj.o.dualErr));
        end
        
        function routineUpdateStep(obj)
            obj.i.rhs = obj.i.b ...
                + obj.p.rho * ( ...
                obj.i.gradX'*(obj.i.vx(:)-obj.i.wx(:)) ...
                + obj.i.gradY'*(obj.i.vy(:)-obj.i.wy(:)) ...
                + obj.i.gradZ'*(obj.i.vz(:)-obj.i.wz(:)) ...
                );
            
            obj.o.result = obj.i.effA \ obj.i.rhs;
            
            obj.i.fx = obj.i.gradX*obj.o.result;
            obj.i.fy = obj.i.gradY*obj.o.result;
            obj.i.fz = obj.i.gradZ*obj.o.result;
            
            obj.i.vxOld = obj.i.vx;
            obj.i.vyOld = obj.i.vy;
            obj.i.vzOld = obj.i.vz;
            
            if strcmp(obj.p.tv, 'aniso')
                obj.i.vx = softThresHold1DVector(obj.i.fx+obj.i.wx,obj.p.alpha/obj.p.rho);
                obj.i.vy = softThresHold1DVector(obj.i.fy+obj.i.wy,obj.p.alpha/obj.p.rho);
                obj.i.vz = softThresHold1DVector(obj.i.fz+obj.i.wz,obj.p.alpha/obj.p.rho);
            else
                [obj.i.vx,obj.i.vy] = softThresHold2DVector(obj.i.fx+obj.i.wx,obj.i.fy+obj.i.wy,obj.p.alpha/obj.p.rho);
            end
            
            obj.i.wx = obj.i.wx + obj.i.fx - obj.i.vx;
            obj.i.wy = obj.i.wy + obj.i.fy - obj.i.vy;
            obj.i.wz = obj.i.wz + obj.i.fz - obj.i.vz;
            
        end
        
        
        
        function adaptiveStep(obj)
            obj.i.rhoOld = obj.p.rho;
            if obj.o.primalErr>5*obj.o.dualErr
                obj.p.rho = obj.p.rho*(1+obj.i.adaptiveWeight);
                if obj.i.adaptiveAdaptiveWeight
                    obj.i.adaptiveWeight = obj.i.adaptiveWeight*obj.i.adaptiveWeightWeight;
                end
                obj.i.adaptiveString = [obj.i.adaptiveString,'^'];
                obj.i.effA = speye(obj.i.N) + obj.p.rho * obj.i.DAD;
            elseif obj.o.dualErr>5*obj.o.primalErr
                obj.p.rho = obj.p.rho/(1+obj.i.adaptiveWeight);
                if obj.i.adaptiveAdaptiveWeight
                    obj.i.adaptiveWeight = obj.i.adaptiveWeight*obj.i.adaptiveWeightWeight;
                end
                obj.i.adaptiveString = [obj.i.adaptiveString,'v'];
                obj.i.effA = speye(obj.i.N) + obj.p.rho * obj.i.DAD;
            else
                obj.i.adaptiveString = [obj.i.adaptiveString,' '];
            end
            
            
            obj.i.wx = obj.i.wx*(obj.i.rhoOld/obj.p.rho);
            obj.i.wy = obj.i.wy*(obj.i.rhoOld/obj.p.rho);
            obj.i.wz = obj.i.wz*(obj.i.rhoOld/obj.p.rho);
        end
    end
end

